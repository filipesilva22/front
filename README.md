# Demo instructions:
#### - Install all dependencies: `npm install`
#### - Change apiUrl in `src/environments/environment.ts` to point to current api URL 
#### - Run the application: `ng serve --open`
