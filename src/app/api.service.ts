import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiURL: string;

  constructor(private http: HttpClient) {
    this.apiURL = environment.apiUrl;
  }

  getJobs(filters?: any): Observable<JSON> {
   
    if(filters){
      var url = this.apiURL + 'jobs?'
      var i = 0;
      for (const [key, value] of Object.entries(filters)) {
        if(i>0){
          url += "&";
        }
        url+= key + "=" + value;
        i++;
      }
      return this.http.get<JSON>(url);
    }
    
    return this.http.get<JSON>(this.apiURL + 'jobs');
  }
  /* getJobs(level:string): Observable<JSON> {
    return this.http.get<JSON>(this.apiURL + 'jobs');
  }
  getJobs(role:string): Observable<JSON> {
    return this.http.get<JSON>(this.apiURL + 'jobs');
  } */
  getLevels(): Observable<JSON> {
    return this.http.get<JSON>(this.apiURL + 'levels');
  }
  getRoles(): Observable<JSON> {
    return this.http.get<JSON>(this.apiURL + 'roles');
  }
}
