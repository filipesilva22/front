import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobsResolver } from './resolvers/jobs.resolver';
import { JobsComponent } from './jobs/jobs.component';
import { LevelsResolver } from './resolvers/levels.resolver';
import { RolesResolver } from './resolvers/roles.resolver';

const routes: Routes = [
  {
    path: '',
    component: JobsComponent,
    resolve: {
      jobs: JobsResolver,
      levels: LevelsResolver,
      roles: RolesResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }