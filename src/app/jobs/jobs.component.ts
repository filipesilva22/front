import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {
  jobs: any;
  levels: any;
  roles: any;
  levelOption: any;
  roleOption: any;
  activeLevel: any;
  activeRole: any;
  filters: any;
  sortOrder: string;
  closeResult = '';



  constructor(private route: ActivatedRoute, private apiService: ApiService, private modalService: NgbModal) {
    this.levelOption = "";
    this.roleOption = "";
    this.activeLevel = "";
    this.activeRole = "";
    this.filters = {};
    this.sortOrder = "";
   }

  ngOnInit(): void {
    this.jobs = this.route.snapshot.data['jobs'];
    this.levels = this.route.snapshot.data['levels'];
    this.roles = this.route.snapshot.data['roles'];
  }

  onLevelChange(id:string){
    
    this.levels.forEach((level: any) => {
      if(level.id == id){
        this.filters["level"] = level.id;
        this.activeLevel = level;
      }
    });

    this.getJobsWithFilters();
  }

  onRoleChange(id:string){
    this.activeRole = id;
    this.roles.forEach((role: any) => {
      if(role.id == id){
        this.filters["role"] = role.id;
        this.activeRole = role;
      }
    });
    this.getJobsWithFilters();
  }


  removeLevelFilter(){
    
    delete this.filters.level;
    this.activeLevel = "";
    this.levelOption = "";
    this.getJobsWithFilters();
  }
  removeRoleFilter(){
    delete this.filters.role;
    this.activeRole = "";
    this.roleOption = "";
    this.getJobsWithFilters();
  }


  getJobsWithFilters(){
    this.apiService.getJobs(this.filters).subscribe((jobs:JSON)=>{
      this.jobs = jobs;
    });
  }

  sort(){
    if(this.sortOrder == "" || this.sortOrder == "desc"){
      this.sortOrder = "asc";
      this.jobs.sort(function(a:any,b:any) {
        var x = a.position.toLowerCase();
        var y = b.position.toLowerCase();
        return x < y ? -1 : x > y ? 1 : 0;
      });
    } else {
      this.sortOrder ="desc";
      this.jobs.sort(function(a:any,b:any) {
        var x = a.position.toLowerCase();
        var y = b.position.toLowerCase();
        return x < y ? 1 : x > y ? -1 : 0;
      });
    }
    
  }

  removeFilters(){
    this.removeLevelFilter();
    this.removeRoleFilter();
  }


  open(content:any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true });
  }

}
