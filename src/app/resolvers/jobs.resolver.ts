import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JobsResolver implements Resolve<JSON> {
  constructor(private apiService: ApiService){}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<JSON> {
    return this.apiService.getJobs();
  }
}
