import { TestBed } from '@angular/core/testing';

import { LevelsResolver } from './levels.resolver';

describe('LevelsResolver', () => {
  let resolver: LevelsResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(LevelsResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
